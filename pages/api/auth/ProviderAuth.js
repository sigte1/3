
import { tokenizer } from "../../../utils/auth/LoginService"



export default async function handler(req, res) {

 const { idApplication, token } =  await tokenizer({ id: 22, clientId: 'apaDev' }); // ParaDev

  // const { idApplication, token } =  await tokenizer({ id: 14, clientId: 'apa' }); // ParaProd

  const authUrl = `http://sso.hcsba.cl?app=${idApplication}&token=${token}`

  res.redirect(authUrl)
}
