import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { getImagen, getPerfil, tokenizer, validTokenizer } from '../../../utils/auth/LoginService';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import { decode } from 'jsonwebtoken';

const jwtSecret = process.env.JWT_SECRET;
export const authOptions = {
    // Configure one or more authentication providers
    secret: process.env.NEXTAUTH_SECRET,
    providers: [
        CredentialsProvider({
            id: 'cred',
            name: 'Credentials',
            credentials: {},
            async authorize(credentials, req) {
                const rut = credentials.user;
                const token = credentials.token;
                const validUser = await validTokenizer(rut, token);
                const perfil = (await getPerfil(rut)) || null;
                console.log(perfil);
                const User = rut;
                const CodigoModulo = 1000;
                const CodigoProyecto = 1000;
                let data = {
                    User: User,
                    CodigoModulo: CodigoModulo,
                    CodigoProyecto: CodigoProyecto
                };
                let Roles = [];
                try {
                    const response = await axios.post('http://wsi.hcsba.cl/authe', data);
                    Roles = response.data.roles;
                    //let values = Object.values(response.data.roles);
                } catch (error) {
                    console.log(error);
                }
                if (Roles.length > 0) {
                    let user;
                    if (perfil) {
                        user = {
                            id: validUser.id,
                            name: validUser.rut,
                            nombre: perfil?.user || 'No registrado',
                            email: perfil?.mail || 'no registrado',
                            image: perfil?.image || null,
                            rol: Roles
                        };
                    } else {
                        user = {
                            id: validUser.id,
                            name: validUser.rut,
                            nombre: 'No registrado',
                            email: 'no registrado',
                            image:  null,
                            rol: Roles
                        };
                    }

                    if (user) {
                        return user;
                    }
                    return null;
                } else {
                    const user = {
                        id: validUser.id,
                        name: validUser.rut,
                        email: validUser.token,
                        image: null,
                        rol: []
                    };
                    if (user) {
                        return user;
                    }
                    return null;
                }
            }
        }),
        {
            id: 'mi-proveedor-de-sso',
            name: 'Mi proveedor de SSO',
            type: 'oauth',
            scope: '',
            clientId: process.env.AUTH0_SECRET,
            authorization: {
                url: '/api/auth/ProviderAuth'
            }
        }
    ],
    callbacks: {
        async signIn({ user, account, credentials }) {
            // console.log(credentials);
            console.log(user);
            if (user) {
                return user;
            } else {
                // Return false to display a default error message
                return '/login';
                // Or you can return a URL to redirect to:
                // return '/unauthorized'
            }
        },
        async redirect(options) {
            return options.baseUrl;
        },
        async jwt({ token, user, account, profile, isNewUser }) {
            if (user?.rol) {
                token.rol = user.rol;
                token.nombre = user?.nombre || 'No informado';
                const newToken = jwt.sign({ id: user.id, email: user.email, rol: user.rol }, jwtSecret, {
                    expiresIn: '5m'
                });
                token.jwt = newToken;
            }
            return token; // Asegúrate de que el token se devuelva siempre
        },
        async session({ session, user, token }) {
            //session.user.image = null;
            if (token) {
                session.roles = token.rol;
                session.jwt = token.jwt;
                token?.nombre ? (session.nombre = token?.nombre) : null;
                const decodedToken = decode(token.jwt);
                const currentTime = Math.floor(Date.now() / 1000);
                const expiresIn = decodedToken.exp - currentTime;
                if (expiresIn < 5) {
                    const newToken = jwt.sign({ id: decodedToken.id, email: decodedToken.email, rol: decodedToken.rol }, jwtSecret, {
                        expiresIn: '5m' // Hacer que el nuevo token JWT expire en 10 segundos
                    });
                    token.jwt = newToken;
                    session.jwt = newToken;
                } else {
                    session.jwt = token.jwt;
                }
            } else {
                session.roles = [];
            }
            // session.roles = token.rol;
            return session;
        }
    },
    pages: {
        signIn: '/api/auth/ProviderAuth'
    },
    session: {
        jwt: true,
        maxAge: 1800
        //cookie: { maxAge: 10, path: "/", sameSite: "lax", timeZone: "America/Santiago" },
    }
};
export default NextAuth(authOptions);
