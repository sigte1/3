import React, {useState} from 'react';
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import CargaMasivaBuscar from "@/components/casos/carga-masiva/seccion-selector/carga-masiva-buscar";

const ListadoCargas = () => {
    const [products, setProducts] = useState([
        {ID: 1, Tipo: 'INGRESO',Servicio:13, Fecha: '2021-08-01', Nombre: 'archivo1', Usuario: 'usuario1', Estado: 'OK', Mensaje: 'mensaje1'},
        {ID: 2, Tipo: 'INGRESO',Servicio:13, Fecha: '2021-08-02', Nombre: 'archivo2', Usuario: 'usuario2', Estado: 'OK', Mensaje: 'mensaje2'},
        {ID: 3, Tipo: 'INGRESO',Servicio:13, Fecha: '2021-08-03', Nombre: 'archivo3', Usuario: 'usuario3', Estado: 'OK', Mensaje: 'mensaje3'},
        {ID: 4, Tipo: 'INGRESO',Servicio:13, Fecha: '2021-08-04', Nombre: 'archivo4', Usuario: 'usuario4', Estado: 'OK', Mensaje: 'mensaje4'},
    ]);
    const header = (
        <CargaMasivaBuscar/>
    )
    return (
        <>

            <DataTable header={header} value={products} editMode="row" dataKey="id" tableStyle={{ minWidth: '50rem' }}>
                <Column field="ID" header="ID"></Column>
                <Column field="Tipo" header="Tipo"></Column>
                <Column field="Servicio" header="Servicio Salud"></Column>
                <Column field="Fecha" header="Fecha Creación"></Column>
                <Column field="Nombre" header="Nombre archivo"></Column>
                <Column field="Usuario" header="Usuario"></Column>
                <Column field="Estado" header="Estado"></Column>
                <Column field="Mensaje" header="Mensaje"></Column>
                <Column field="Tipo" header="Tipo"></Column>

            </DataTable>

        </>

    );
};

export default ListadoCargas;
