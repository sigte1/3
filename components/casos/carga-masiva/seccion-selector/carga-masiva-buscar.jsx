import React, {useState} from 'react';
import {Card} from "primereact/card";
import {Button} from "primereact/button";
import {Controller, useForm} from "react-hook-form";
import {InputText} from "primereact/inputtext";
import {classNames} from 'primereact/utils';
import {Dropdown} from "primereact/dropdown";
import {Calendar} from "primereact/calendar";
import {locale, addLocale} from 'primereact/api';
import {Dialog} from "primereact/dialog";


//#region  fecha español
addLocale('es', {
    firstDayOfWeek: 1,
    dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
    dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
    monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    today: 'Hoy',
    clear: 'Limpiar'
});

locale('es');
//#endregion

const CargaMasivaBuscar = () => {
    const [showDialogCargaMasiva, setShowDialogCargaMasiva] = useState(false);
    const defaultValues = {};

    const {
        control, formState: {errors}, handleSubmit, reset
    } = useForm({defaultValues});
    const submitCargaMasiva = (data) => {
        console.log(data);
    }
    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> :
            <small className="p-error">&nbsp;</small>;
    };
    const servicosSalud = [{name: 'Servicio de Salud Arica', code: 'ARICA'}, {
        name: 'Servicio de Salud Iquique',
        code: 'IQUIQUE'
    }, {name: 'Servicio de Salud Antofagasta', code: 'ANTOFAGASTA'}, {
        name: 'Servicio de Salud Atacama',
        code: 'ATACAMA'
    }, {name: 'Servicio de Salud Coquimbo', code: 'COQUIMBO'}, {
        name: 'Servicio de Salud Valparaíso - San Antonio',
        code: 'VALPARAISO'
    }, {
        name: 'Servicio de Salud Metropolitano Central',
        code: 'METROPOLITANO'
    }, {name: 'Servicio de Salud Metropolitano Norte', code: 'NORTE'}, {
        name: 'Servicio de Salud Metropolitano Oriente',
        code: 'ORIENTE'
    }, {name: 'Servicio de Salud Metropolitano Sur', code: 'SUR'}, {
        name: 'Servicio de Salud Metropolitano Sur Oriente',
        code: 'SURORIENTE'
    }, {name: 'Servicio de Salud del Maule', code: 'MAULE'}, {name: 'Servicio de Salud Ñuble', code: 'ÑUBLE'},

    ]
    const tipoCarga = [{name: 'Ingreso', id: 1}, {name: 'Edición', id: 2}, {name: 'Egreso', id: 3},]
    return (<>
            <Card>
                <div className="flex flex-row flex-wrap">
                    <div
                        className="text-xl flex align-items-center justify-content-center  font-bold border-round m-2">Cargas
                        Masivas
                    </div>
                    <div
                        className="flex align-items-center justify-content-center w-15rem h-4rem  font-bold border-round m-2">
                        <Button icon="pi pi-plus" label="Nueva carga" severity="success" onClick={() => setShowDialogCargaMasiva(true)}/>
                    </div>
                </div>
                <form className="col-12" onSubmit={handleSubmit(submitCargaMasiva)}>
                    <div className="p-fluid formgrid grid">
                        <div className="field col-3 md:col6">
                            <Controller
                                name="servicio_salud"
                                control={control}
                                render={({field, fieldState}) => (<Dropdown
                                        id={field.name}
                                        value={field.value}
                                        optionLabel="name"
                                        placeholder="Servicio salud"
                                        options={servicosSalud}
                                        focusInputRef={field.ref}
                                        onChange={(e) => field.onChange(e.value)}
                                        className={'p-inputtext-sm'+classNames({'p-invalid': fieldState.error})}
                                    />)}
                            />
                            {getFormErrorMessage('servicio_salud')}
                        </div>
                        <div className="field col-3 md:col6">
                            <Controller
                                name="servicio_salud"
                                control={control}
                                render={({field, fieldState}) => (<Dropdown
                                        id={field.name}
                                        value={field.value}
                                        optionLabel="name"
                                        placeholder="Filtrar por tipo de carga"
                                        options={tipoCarga}
                                        focusInputRef={field.ref}
                                        onChange={(e) => field.onChange(e.value)}
                                        className={'p-inputtext-sm'+classNames({'p-invalid': fieldState.error})}
                                    />)}
                            />
                            {getFormErrorMessage('servicio_salud')}
                        </div>
                        <div className="field col-3 md:col6">
                            <Controller
                                name="servicio_salud"
                                control={control}
                                render={({field, fieldState}) => (<Dropdown
                                        id={field.name}
                                        value={field.value}
                                        optionLabel="name"
                                        placeholder="Filtrar por estado de carga"
                                        options={servicosSalud}
                                        focusInputRef={field.ref}
                                        onChange={(e) => field.onChange(e.value)}
                                        className={'p-inputtext-sm'+classNames({'p-invalid': fieldState.error})}
                                    />)}
                            />
                            {getFormErrorMessage('servicio_salud')}
                        </div>
                        <div className="field col-3 md:col6">
                            <Button size="small" type="button" label="Borrar" severity="secondary" text raised/>
                        </div>
                        <div className="field col-3 md:col6">

                            <Controller
                                name="date"
                                control={control}
                                render={({field, fieldState}) => (<>
                                        <Calendar showIcon selectionMode="range" inputId={field.name} placeholder="Rango Fechas"
                                                  value={field.value} onChange={field.onChange} dateFormat="dd/mm/yy"
                                                  className={'p-inputtext-sm'+classNames({'p-invalid': fieldState.error})}/>
                                        {getFormErrorMessage(field.name)}
                                    </>)}
                            />
                        </div>
                        <div className="field col-3 md:col6">

                        </div>
                        <div className="field col-3 md:col6">
                            <Controller
                                name="value"
                                control={control}
                                render={({field, fieldState}) => (<>

                                        <span className="p-input-icon-left">
                                            <i className="pi pi-search" />
                                        <InputText id={field.name} value={field.value}
                                                   placeholder="Búsqueda"
                                                   className={'p-inputtext-sm'+classNames({'p-invalid': fieldState.error})}
                                                   onChange={(e) => field.onChange(e.target.value)}/>
                                        </span>

                                        {getFormErrorMessage(field.name)}
                                    </>)}
                            />
                        </div>
                        <div className="field col-3 md:col6">
                            <Button label="Buscar" severity="info" raised size="small"/>
                        </div>
                    </div>
                </form>
            </Card>

            <Dialog header="Carga Nueva" position="top" visible={showDialogCargaMasiva} onHide={() => setShowDialogCargaMasiva(false)}
                    style={{ width: '50vw' }} breakpoints={{ '960px': '75vw', '641px': '100vw' }}>

            </Dialog>

        </>);
};

export default CargaMasivaBuscar;
