/* eslint-disable react/display-name */
import { signOut, useSession } from 'next-auth/react';
import getConfig from 'next/config';
import Link from 'next/link';
import Router, { useRouter } from 'next/router';
import { classNames } from 'primereact/utils';
import React, { forwardRef, useContext, useImperativeHandle, useRef } from 'react';
import { LayoutContext } from './context/layoutcontext';
import { Toast } from 'primereact/toast';
import { Menu } from 'primereact/menu';

const AppTopbar = forwardRef((props, ref) => {
    const { data: session } = useSession();
    const { layoutConfig, layoutState, onMenuToggle, showProfileSidebar } = useContext(LayoutContext);
    const menubuttonRef = useRef(null);
    const topbarmenuRef = useRef(null);
    const topbarmenubuttonRef = useRef(null);
    const contextPath = getConfig().publicRuntimeConfig.contextPath;

    useImperativeHandle(ref, () => ({
        menubutton: menubuttonRef.current,
        topbarmenu: topbarmenuRef.current,
        topbarmenubutton: topbarmenubuttonRef.current
    }));
    const menuLeft = useRef(null);
    const menuRight = useRef(null);
    const router = useRouter();
    const toast = useRef(null);
    const items = [
        {
            items: [
                {
                    label: 'Bandeja',
                    command: () => {
                        toast.current.show({ severity: 'success', summary: 'Updated', detail: 'Data Updated', life: 3000 });
                    }
                },
                {
                    label: 'Carga Masiva',
                    command: () => {
                        router.push('/casos/cargas');
                    }
                },
                {
                    label: 'Historial Caso',
                    command: () => {
                        toast.current.show({ severity: 'warn', summary: 'Delete', detail: 'Data Deleted', life: 3000 });
                    }
                },
                {
                    label: 'Consulta por Run',
                    command: () => {
                        toast.current.show({ severity: 'warn', summary: 'Delete', detail: 'Data Deleted', life: 3000 });
                    }
                },
            ]
        },
    ];
    return (
        <>

        <div className="layout-topbar">
        <Toast ref={toast}></Toast>
            <Link legacyBehavior href="/">
                <a className="layout-topbar-logo" >
                    <>
                        <div className='flex justify-content-around align-items-center'>
                        <span className='text-xl font-italic font-bold' >Dashboard</span>
                        </div>
                    </>
                </a>
            </Link>
            <Link legacyBehavior href="">
                <a className="layout-topbar-logo" onClick={(event) => menuRight.current.toggle(event)}>
                    <>
                        <div className='flex justify-content-around align-items-center'>
                            <Menu model={items} popup ref={menuRight} id="popup_menu_left" />
                            <span className='text-xl font-italic font-bold' >Casos</span>
                        </div>
                    </>
                </a>
            </Link>
            <Link legacyBehavior href="">
                <a className="layout-topbar-logo">
                    <>
                        <div className='flex justify-content-around align-items-center'>
                            <span className='text-xl font-italic font-bold'>Maestros</span>
                        </div>
                    </>
                </a>
            </Link>
            <Link legacyBehavior href="">
                <a className="layout-topbar-logo">
                    <>
                        <div className='flex justify-content-around align-items-center'>
                            <span className='text-xl font-italic font-bold'>Reportes</span>
                        </div>
                    </>
                </a>
            </Link>
            <Link legacyBehavior href="">
                <a className="layout-topbar-logo">
                    <>
                        <div className='flex justify-content-around align-items-center'>
                            <span className='text-xl font-italic font-bold'>Tutorial</span>
                        </div>
                    </>
                </a>
            </Link>
            <Link legacyBehavior href="">
                <a className="layout-topbar-logo">
                    <>
                        <div className='flex justify-content-around align-items-center'>
                            <span className='text-xl font-italic font-bold'>Administración</span>
                        </div>
                    </>
                </a>
            </Link>

            {/* <button ref={menubuttonRef} type="button" className="p-link layout-menu-button layout-topbar-button" onClick={onMenuToggle}>
                <i className="pi pi-bars" />
            </button> */}

            <button ref={topbarmenubuttonRef} type="button" className="p-link layout-topbar-menu-button layout-topbar-button" onClick={showProfileSidebar}>
                <i className="pi pi-ellipsis-v" />
            </button>
            {/* botones de la derecha */}
            <div
                ref={topbarmenuRef}
                className={classNames('layout-topbar-menu', {
                    'layout-topbar-menu-mobile-active': layoutState.profileSidebarVisible
                })}
                >
                <Link legacyBehavior href="/Account">
                    <button type="button" className="p-link layout-topbar-button">
                        <i className="pi pi-user"></i>
                        <span>Profile</span>
                    </button>
                </Link>
                <Link legacyBehavior href="/Mantenedores">
                    <button type="button" className="p-link layout-topbar-button">
                        <i className="pi pi-cog"></i>
                        <span>Settings</span>
                    </button>
                </Link>

                {session && (
                    <button type="button" onClick={() => signOut({ baseUrl: '/login' })} className="p-link layout-topbar-button">
                        <i className="pi pi-sign-out"></i>
                        <span>Calendar</span>
                    </button>
                )}
            </div>
        </div>
                </>
    );
});

export default AppTopbar;
